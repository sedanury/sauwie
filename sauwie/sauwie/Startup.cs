﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sauwie.Startup))]
namespace sauwie
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
